#!/bin/bash

set -eo pipefail

declare -A cmd=(
	[apache]='apache2-foreground'
	[fpm]='php-fpm'
	[fpm-alpine]='php-fpm'
)

# version_greater_or_equal A B returns whether A >= B
function version_greater_or_equal() {
	[[ "$(printf '%s\n' "$@" | sort -n | head -n 1)" != "$1" || "$1" == "$2" ]];
}

latests=( $(curl -sSL 'https://nextcloud.com/changelog/' |tac|tac| \
	grep -o "\(Version\|Release\)\s\+[[:digit:]]\+\(\.[[:digit:]]\+\)\+" | \
	awk '{ print $2 }' | sort -n ) )

travisEnv=
for latest in "${latests[@]}"; do
	version=$(echo "$latest" | cut -d. -f1-2)

	for variant in apache fpm fpm-alpine; do
		# Create the version+variant directory with a Dockerfile.
		mkdir -p "build/$version/$variant"

        # Copy the appropriate Dockerfile template
		template="Dockerfile.template"
        if [ "$variant" == "fpm-alpine" ]; then
		    template="Dockerfile-alpine.template"
        fi
		cp "$template" "build/$version/$variant/Dockerfile"

        # Define extension versions for PHP-5.6
        php="5.6"
        apcu="4.0.10"
        memcached="2.2.0"
        mysql="mysql"
        redis="2.2.8"

        # If version >= 11.0, define extension versions for PHP-7.1
		if version_greater_or_equal "$version" "11.0"; then
            php="7.1"
            apcu="5.1.8"
            memcached="3.0.2"
            mysql="mysqli"
            redis="3.1.1"
		fi

        # Define NextCloud pgp release key
        release_key="28806A878AE423A28372792ED75899B9A724937A"

		echo "updating $latest [$version] $variant"

		# Replace the variables.
		sed -ri -e '
            s/%%PHP%%/'"$php"'/g;
            s/%%APCU%%/'"$apcu"'/g;
            s/%%REDIS%%/'"$redis"'/g;
            s/%%MEMCACHED%%/'"$memcached"'/g;
            s/%%MYSQL%%/'"$mysql"'/g;
			s/%%VARIANT%%/'"$variant"'/g;
			s/%%VERSION%%/'"$latest"'/g;
			s/%%CMD%%/'"${cmd[$variant]}"'/g;
			s/%%RELEASE_KEY%%/'"$release_key"'/g;
		' "build/$version/$variant/Dockerfile"

		# Remove Apache commands if we're not an Apache variant.
		if [ "$variant" != "apache" ]; then
			sed -ri -e '/a2enmod/d' "build/$version/$variant/Dockerfile"
		fi

		# Copy the docker-entrypoint.
		cp docker-entrypoint.sh "build/$version/$variant/docker-entrypoint.sh"

	done
done
